# Gym API

This API is provides the functionality to create classes and book attendance for classes.

## Tech details

This application is built using `golang v17` and using `go mod` as dependency management.

Another important point is that it uses `uber/fx` as dependency injection, which allows
each module to be self sufficient, and avoid complexity on application wire up.

## Running locally

> You must have golang v1.17 installed to run this application.

To download the dependencies you can run `go mod download`

Using the make file, you can execute `make run` to run the application, or execute 
`go run cmd/api/*.go` on this repo root folder.

The application runs by default at HTTP port 8080 and has a `/ping` endpoint that allows 
you to check if it is running after start up.

### Make Commands

#### make run

Run the application locally

#### make test

Run unit tests locally

#### make it

Run integration tests locally