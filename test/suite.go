//go:build integration
// +build integration

package test

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"gitlab.com/felipecaputo/gym-api/cmd/api/modules"
)

const CTJSON = "application/json"

var nextHttpPort = 8001

type (
	App interface {
		Start(ctx context.Context) error
		Stop(ctx context.Context) error
	}

	E2ESuite struct {
		suite.Suite

		app    App
		Ctx    context.Context
		Client http.Client
		port   int
	}
)

func TestSuite(t *testing.T) {
	suite.Run(t, &E2ESuite{})
}

func (s *E2ESuite) SetupTest() {
	s.Ctx = context.Background()
	s.port = nextHttpPort
	nextHttpPort++

	s.app = modules.NewApp(s.port)
	s.Client = http.Client{
		Timeout: 50 * time.Millisecond,
	}

	if err := s.app.Start(s.Ctx); err != nil {
		s.FailNow(err.Error())
	}
}

func (s *E2ESuite) TearDownTest() {
	if err := s.app.Stop(s.Ctx); err != nil {
		s.FailNow(err.Error())
	}
}

func (s *E2ESuite) ToJSON(obj interface{}) io.Reader {
	json, err := json.Marshal(obj)
	if err != nil {
		s.FailNow(err.Error())
		return nil
	}

	return bytes.NewReader(json)
}

func (s *E2ESuite) GetBody(r *http.Response) string {
	defer r.Body.Close()

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		s.FailNow(err.Error())
	}

	return string(body)
}

func (s *E2ESuite) GetURL(path string) string {
	return fmt.Sprintf("http://localhost:%d%s", s.port, path)
}
