package classes

import "time"

type Class struct {
	ID        int       `validate:"required,gt=0"`
	Name      string    `validate:"required,min=1"`
	StartDate time.Time `validate:"required"`
	EndDate   time.Time `validate:"required"`
	Capacity  int       `validate:"required,gt=0"`
}
