//go:build integration
// +build integration

package integrationtest

import (
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/felipecaputo/gym-api/test"
)

type CreateClass struct {
	test.E2ESuite
}

func TestSuite(t *testing.T) {
	suite.Run(t, &CreateClass{})
}

func (s *CreateClass) Test_CreateClass_Successfully() {
	const (
		classInput  = "{\"name\":\"Yoga\",\"start_date\":\"2022-12-01T00:00:00Z\",\"end_date\":\"2022-12-20T00:00:00Z\", \"capacity\": 20}"
		classOutput = "{\"name\":\"Yoga\",\"start_date\":\"2022-12-01T00:00:00Z\",\"end_date\":\"2022-12-20T00:00:00Z\",\"capacity\":20,\"id\":1}\n"
	)

	resp, err := http.DefaultClient.Post(s.GetURL("/classes"), test.CTJSON, strings.NewReader(classInput))

	s.Nil(err)

	s.Equal(http.StatusCreated, resp.StatusCode)
	s.JSONEq(classOutput, s.GetBody(resp))
}

func (s *CreateClass) Test_CreateClass_BadRequest() {
	const (
		classInput  = "{\"start_date\":\"2022-12-01T00:00:00Z\",\"end_date\":\"2022-12-20T00:00:00Z\", \"capacity\": 20}"
		classOutput = "{\"error\":\"invalid request: invalid field values: Name: \\\"\\\"\"}"
	)

	resp, err := http.DefaultClient.Post(s.GetURL("/classes"), test.CTJSON, strings.NewReader(classInput))

	s.Nil(err)

	s.Equal(http.StatusBadRequest, resp.StatusCode)
	s.JSONEq(classOutput, s.GetBody(resp))
}
