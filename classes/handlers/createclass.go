package handlers

import (
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"gitlab.com/felipecaputo/gym-api/classes"
	"gitlab.com/felipecaputo/gym-api/internal/httputils"
)

type (
	CreatorService interface {
		CreateClass(name string, startDate, endDate time.Time, capacity int) (classes.Class, error)
	}

	Validator interface {
		IsValidationError(err error) bool
	}

	CreateClassHandler struct {
		creatorService CreatorService
		validator      Validator
	}

	createClassRequestDTO struct {
		Name      string    `json:"name"`
		StartDate time.Time `json:"start_date"`
		EndDate   time.Time `json:"end_date"`
		Capacity  int       `json:"capacity"`
	}

	createClassResponseDTO struct {
		createClassRequestDTO

		ID int `json:"id"`
	}
)

func NewCreateClassHandler(creatorService CreatorService, validator Validator) CreateClassHandler {
	return CreateClassHandler{
		creatorService: creatorService,
		validator:      validator,
	}
}

func RegisterCreateClassEndpoints(r chi.Router, h CreateClassHandler) {
	r.Post("/classes", h.CreateClass)
}

func (h CreateClassHandler) CreateClass(w http.ResponseWriter, r *http.Request) {
	data := createClassRequestDTO{}
	if err := render.Bind(r, &data); err != nil {
		render.Render(w, r, httputils.ErrBadRequest(err))
		return
	}

	class, err := h.creatorService.CreateClass(data.Name, data.StartDate, data.EndDate, data.Capacity)
	if err != nil && h.validator.IsValidationError(err) {
		render.Render(w, r, httputils.ErrBadRequest(err))
		return
	}

	if err != nil {
		render.Render(w, r, httputils.ErrServerError(err))
		return
	}

	render.Status(r, http.StatusCreated)
	render.Render(w, r, createClassResponseDTO{
		ID: class.ID,
		createClassRequestDTO: createClassRequestDTO{
			Name:      class.Name,
			StartDate: class.StartDate,
			EndDate:   class.EndDate,
			Capacity:  class.Capacity,
		},
	})
}

func (d *createClassRequestDTO) Bind(r *http.Request) error {
	return nil
}

func (c createClassResponseDTO) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
