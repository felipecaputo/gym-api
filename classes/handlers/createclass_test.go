package handlers

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/felipecaputo/gym-api/classes"
	"gitlab.com/felipecaputo/gym-api/classes/handlers/mocks"
)

func TestCreateClassHandler_CreateClass(t *testing.T) {
	tests := []struct {
		name           string
		creatorService CreatorService
		validator      Validator
		request        string
		wantStatus     int
		wantBody       string
	}{
		{
			name:       "should return bad request when body isn't json",
			request:    "not json",
			wantStatus: http.StatusBadRequest,
			wantBody:   "\"error\":\"invalid request:",
		},
		{
			name:       "should return bad request when request end_date is invalid",
			request:    "{\"name\": \"Yoga\", \"start_date\": \"2021-12-01T00:00:00Z\", \"end_date\": \"invalid date\", \"capacity\": 20}",
			wantStatus: http.StatusBadRequest,
			wantBody:   "{\"error\":\"invalid request: parsing time \\\"\\\\\\\"invalid date\\\\\\\"",
		},
		{
			name:           "should return bad request when creator returns a validation error",
			request:        "{\"name\": \"Yoga\", \"start_date\": \"2021-12-01T00:00:00Z\", \"end_date\": \"2021-12-01T00:00:00Z\", \"capacity\": 20}",
			wantStatus:     http.StatusBadRequest,
			creatorService: buildCreatorService(classes.Class{}, errors.New("validation error")),
			validator:      buildValidator(true),
			wantBody:       "{\"error\":\"invalid request: validation error",
		},
		{
			name:           "should return internal error when creator failed",
			request:        "{\"name\": \"Yoga\", \"start_date\": \"2021-12-01T00:00:00Z\", \"end_date\": \"2021-12-01T00:00:00Z\", \"capacity\": 20}",
			wantStatus:     http.StatusInternalServerError,
			creatorService: buildCreatorService(classes.Class{}, errors.New("internal error")),
			validator:      buildValidator(false),
			wantBody:       "{\"error\":\"request failed: internal error\"}\n",
		},
		{
			name:           "should return success and class data on successfull request",
			request:        "{\"name\": \"Yoga\", \"start_date\": \"2021-12-01T00:00:00Z\", \"end_date\": \"2021-12-01T00:00:00Z\", \"capacity\": 20}",
			wantStatus:     http.StatusCreated,
			creatorService: buildCreatorService(classes.Class{ID: 1, Name: "Yoga", Capacity: 20}, nil),
			wantBody:       "{\"name\":\"Yoga\",\"start_date\":\"0001-01-01T00:00:00Z\",\"end_date\":\"0001-01-01T00:00:00Z\",\"capacity\":20,\"id\":1}\n",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			createHandler := NewCreateClassHandler(tt.creatorService, tt.validator)
			req, err := http.NewRequest("POST", "/classes", strings.NewReader(tt.request))
			if err != nil {
				t.Fatal(err)
			}

			req.Header.Add("Content-Type", "application/json")

			rr := httptest.NewRecorder()
			handler := http.HandlerFunc(createHandler.CreateClass)
			handler.ServeHTTP(rr, req)

			assert.Equal(t, tt.wantStatus, rr.Code)
			assert.Contains(t, rr.Body.String(), tt.wantBody)
		})
	}
}

func buildValidator(isValidationError bool) Validator {
	m := mocks.Validator{}
	m.On("IsValidationError", mock.Anything).Return(isValidationError)

	return &m
}

func buildCreatorService(class classes.Class, err error) CreatorService {
	m := mocks.CreatorService{}
	m.On("CreateClass", mock.AnythingOfType("string"), mock.AnythingOfType("time.Time"), mock.AnythingOfType("time.Time"),
		mock.AnythingOfType("int")).Return(class, err)

	return &m
}
