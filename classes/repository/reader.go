package repository

import "gitlab.com/felipecaputo/gym-api/classes"

type Reader struct {
	data map[int]classes.Class
}

// NewReader returns a new instace of a Class Reader repository
func NewReader(data map[int]classes.Class) Reader {
	return Reader{
		data: data,
	}
}

// FindByID returns a class if found or a zero value struct if not
func (r Reader) FindByID(classID int) (classes.Class, error) {
	class, ok := r.data[classID]
	if !ok {
		return classes.Class{}, nil
	}

	// the error in signature was kept for consistency, if we were not using an errorless storage
	return class, nil
}
