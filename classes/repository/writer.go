package repository

import (
	"time"

	"gitlab.com/felipecaputo/gym-api/classes"
)

type (
	//go:generate mockery --name Validator
	Validator interface {
		Validate(data interface{}) error
	}

	Writer struct {
		validator Validator
		nextID    int
		data      map[int]classes.Class
	}
)

func NewWriter(validator Validator, data map[int]classes.Class) Writer {
	return Writer{
		validator: validator,
		nextID:    1,
		data:      data,
	}
}

// Create inserts a new class record into the in memory "database"
func (w *Writer) Create(name string, startDate, endDate time.Time, capacity int) (classes.Class, error) {
	class := classes.Class{
		ID:        w.nextID,
		Name:      name,
		StartDate: startDate,
		EndDate:   endDate,
		Capacity:  capacity,
	}

	if err := w.validator.Validate(class); err != nil {
		return classes.Class{}, err
	}

	w.data[w.nextID] = class
	w.nextID++

	return class, nil
}
