package repository

import (
	"errors"
	"reflect"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"gitlab.com/felipecaputo/gym-api/classes"
	"gitlab.com/felipecaputo/gym-api/classes/repository/mocks"
)

func TestWriter_Create(t *testing.T) {
	type args struct {
		name      string
		startDate time.Time
		endDate   time.Time
		capacity  int
	}
	tests := []struct {
		name      string
		args      args
		validator Validator
		want      classes.Class
		wantErr   bool
	}{
		{
			name:      "should create a class correctly",
			validator: buildValidatorMock(nil),
			args: args{
				name:      "Pilates",
				startDate: time.Date(2021, 12, 01, 0, 0, 0, 0, time.UTC),
				endDate:   time.Date(2021, 12, 20, 0, 0, 0, 0, time.UTC),
				capacity:  20,
			},
			want: classes.Class{
				ID:        1,
				Name:      "Pilates",
				StartDate: time.Date(2021, 12, 01, 0, 0, 0, 0, time.UTC),
				EndDate:   time.Date(2021, 12, 20, 0, 0, 0, 0, time.UTC),
				Capacity:  20,
			},
			wantErr: false,
		},
		{
			name:      "should return an error if validate fails",
			validator: buildValidatorMock(errors.New("Some error")),
			args: args{
				name:      "Pilates",
				startDate: time.Date(2021, 12, 01, 0, 0, 0, 0, time.UTC),
				endDate:   time.Date(2021, 12, 20, 0, 0, 0, 0, time.UTC),
				capacity:  20,
			},
			want:    classes.Class{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := NewWriter(tt.validator, map[int]classes.Class{})
			got, err := w.Create(tt.args.name, tt.args.startDate, tt.args.endDate, tt.args.capacity)
			if (err != nil) != tt.wantErr {
				t.Errorf("Writer.Create() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Writer.Create() = %v, want %v", got, tt.want)
			}
		})
	}
}

func buildValidatorMock(validateResult error) Validator {
	m := &mocks.Validator{}
	m.On("Validate", mock.AnythingOfType("classes.Class")).Return(validateResult)

	return m
}
