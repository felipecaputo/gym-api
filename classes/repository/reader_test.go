package repository

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/felipecaputo/gym-api/classes"
)

func TestReader_FindByID(t *testing.T) {
	type args struct {
		classID int
	}
	tests := []struct {
		name      string
		data      map[int]classes.Class
		args      args
		want      classes.Class
		wantedErr string
	}{
		{
			name:      "should return zero value when not found",
			data:      map[int]classes.Class{2: {ID: 2, Name: "Yoga", Capacity: 10, StartDate: time.Date(2021, 12, 01, 0, 0, 0, 0, time.UTC), EndDate: time.Date(2021, 12, 20, 0, 0, 0, 0, time.UTC)}},
			args:      args{classID: 1},
			want:      classes.Class{},
			wantedErr: "",
		},
		{
			name:      "should return the found class when found",
			data:      map[int]classes.Class{2: {ID: 2, Name: "Yoga", Capacity: 10, StartDate: time.Date(2021, 12, 01, 0, 0, 0, 0, time.UTC), EndDate: time.Date(2021, 12, 20, 0, 0, 0, 0, time.UTC)}},
			args:      args{classID: 2},
			want:      classes.Class{ID: 2, Name: "Yoga", Capacity: 10, StartDate: time.Date(2021, 12, 01, 0, 0, 0, 0, time.UTC), EndDate: time.Date(2021, 12, 20, 0, 0, 0, 0, time.UTC)},
			wantedErr: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := NewReader(tt.data)

			got, err := r.FindByID(tt.args.classID)

			assert.Equal(t, tt.want, got)
			if tt.wantedErr == "" {
				assert.Nil(t, err)
			} else {
				assert.EqualError(t, err, tt.wantedErr)
			}
		})
	}
}
