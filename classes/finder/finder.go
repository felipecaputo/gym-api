package finder

import "gitlab.com/felipecaputo/gym-api/classes"

type (
	ClassReader interface {
		FindByID(classID int) (classes.Class, error)
	}

	Service struct {
		reader ClassReader
	}
)

func NewService(reader ClassReader) Service {
	return Service{
		reader: reader,
	}
}

func (s Service) FindByID(classID int) (classes.Class, error) {
	return s.reader.FindByID(classID)
}
