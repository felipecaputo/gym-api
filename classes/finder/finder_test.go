package finder

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/felipecaputo/gym-api/classes"
	"gitlab.com/felipecaputo/gym-api/classes/finder/mocks"
)

func TestService_FindByID(t *testing.T) {
	type args struct {
		classID int
	}
	tests := []struct {
		name      string
		reader    ClassReader
		args      args
		want      classes.Class
		wantedErr string
	}{
		{
			name:      "should return the serice returned data",
			reader:    buildReader(classes.Class{ID: 10}, nil),
			args:      args{classID: 1},
			want:      classes.Class{ID: 10},
			wantedErr: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewService(tt.reader)
			got, err := s.FindByID(tt.args.classID)

			assert.Equal(t, tt.want, got)
			if tt.wantedErr == "" {
				assert.Nil(t, err)
			} else {
				assert.EqualError(t, err, tt.wantedErr)
			}
		})
	}
}

func buildReader(class classes.Class, err error) ClassReader {
	m := mocks.ClassReader{}
	m.On("FindByID", mock.AnythingOfType("int")).Return(class, err)

	return &m
}
