package creator

import (
	"time"

	"gitlab.com/felipecaputo/gym-api/classes"
)

type (
	ClassCreator interface {
		Create(name string, startDate, endDate time.Time, capacity int) (classes.Class, error)
	}

	Service struct {
		creator ClassCreator
	}
)

func NewService(creator ClassCreator) Service {
	return Service{
		creator: creator,
	}
}

func (s Service) CreateClass(name string, startDate, endDate time.Time, capacity int) (classes.Class, error) {
	// can have service level validations, like account type, valid license and so on
	return s.creator.Create(name, startDate, endDate, capacity)
}
