package creator

import (
	"errors"
	"reflect"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"gitlab.com/felipecaputo/gym-api/classes"
	"gitlab.com/felipecaputo/gym-api/classes/creator/mocks"
)

func TestService_CreateClass(t *testing.T) {
	date := time.Date(2021, 12, 1, 0, 0, 0, 0, time.UTC)
	type args struct {
		name      string
		startDate time.Time
		endDate   time.Time
		capacity  int
	}
	tests := []struct {
		name    string
		creator ClassCreator
		args    args
		want    classes.Class
		wantErr bool
	}{
		{
			name:    "should return error when creator returns error",
			creator: buildClassCreator(classes.Class{}, errors.New("some error")),
			args:    args{name: "Yoga", startDate: time.Now(), endDate: time.Now(), capacity: 20},
			wantErr: true,
		},
		{
			name:    "should return a class when creator returns it",
			creator: buildClassCreator(classes.Class{ID: 20, Name: "Yoga", StartDate: date, EndDate: date.Add(24 * time.Hour), Capacity: 20}, nil),
			args:    args{name: "Yoga", startDate: time.Now(), endDate: time.Now(), capacity: 20},
			want:    classes.Class{ID: 20, Name: "Yoga", StartDate: date, EndDate: date.Add(24 * time.Hour), Capacity: 20},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewService(tt.creator)

			got, err := s.CreateClass(tt.args.name, tt.args.startDate, tt.args.endDate, tt.args.capacity)
			if (err != nil) != tt.wantErr {
				t.Errorf("Service.CreateClass() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Service.CreateClass() = %v, want %v", got, tt.want)
			}
		})
	}
}

func buildClassCreator(class classes.Class, err error) ClassCreator {
	m := &mocks.ClassCreator{}
	m.On("Create", mock.AnythingOfType("string"), mock.AnythingOfType("time.Time"), mock.AnythingOfType("time.Time"),
		mock.AnythingOfType("int")).Return(class, err)

	return m
}
