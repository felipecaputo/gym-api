package modules

import (
	"gitlab.com/felipecaputo/gym-api/bookings/handlers"
	"gitlab.com/felipecaputo/gym-api/bookings/repository"
	"gitlab.com/felipecaputo/gym-api/bookings/scheduler"
	"gitlab.com/felipecaputo/gym-api/classes/finder"
	"gitlab.com/felipecaputo/gym-api/internal/validator"
	"go.uber.org/fx"
)

var bookingsFactories = fx.Provide(
	repository.NewWriter,
	scheduler.NewService,
	handlers.NewSingleClassHandler,

	func(v validator.Validator) repository.Validator { return v },
	func(v validator.Validator) handlers.Validator { return v },
	func(r repository.Writer) scheduler.BookingCreator { return &r },
	func(f finder.Service) scheduler.ClassFinder { return f },
	func(s scheduler.Service) handlers.SchedulerService { return s },
)

var Bookings = fx.Options(
	bookingsFactories,
	fx.Invoke(handlers.RegisterScheduleEndpoints),
)
