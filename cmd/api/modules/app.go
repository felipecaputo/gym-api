package modules

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"go.uber.org/fx"
)

func NewApp(port int) *fx.App {
	return fx.New(
		Internal,
		Classes,
		Bookings,
		fx.Invoke(func(lc fx.Lifecycle, router chi.Router) { startHttpServer(lc, port, router) }),
	)
}

func startHttpServer(lc fx.Lifecycle, port int, router chi.Router) {
	server := http.Server{
		Addr:    fmt.Sprintf(":%d", port),
		Handler: router,
	}
	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			fmt.Println("Starting HTTP Server")
			go server.ListenAndServe()
			return nil
		},
		OnStop: func(ctx context.Context) error {
			return server.Shutdown(ctx)
		},
	})
}
