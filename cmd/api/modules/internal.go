// package modules contains all the application and infraestructure code organized in Fx Modules to be injected in the application
package modules

import (
	"gitlab.com/felipecaputo/gym-api/internal/httprouter"
	"gitlab.com/felipecaputo/gym-api/internal/validator"
	"go.uber.org/fx"
)

var Internal = fx.Provide(
	httprouter.NewRouter,
	validator.NewValidator,
)
