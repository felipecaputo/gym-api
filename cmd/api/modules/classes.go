package modules

import (
	"gitlab.com/felipecaputo/gym-api/classes"
	"gitlab.com/felipecaputo/gym-api/classes/creator"
	"gitlab.com/felipecaputo/gym-api/classes/finder"
	"gitlab.com/felipecaputo/gym-api/classes/handlers"
	"gitlab.com/felipecaputo/gym-api/classes/repository"
	"gitlab.com/felipecaputo/gym-api/internal/validator"
	"go.uber.org/fx"
)

var classesFactories = fx.Provide(
	func() map[int]classes.Class { return map[int]classes.Class{} }, // classes storage

	// here we register all out concrete constructors
	repository.NewWriter,
	repository.NewReader,
	creator.NewService,
	finder.NewService,
	handlers.NewCreateClassHandler,

	// here we make the relation between the consumer interface and real implementation
	func(v validator.Validator) repository.Validator { return v },
	func(w repository.Writer) creator.ClassCreator { return &w },
	func(s creator.Service) handlers.CreatorService { return s },
	func(v validator.Validator) handlers.Validator { return v },
	func(f repository.Reader) finder.ClassReader { return f },
)

var Classes = fx.Options(
	classesFactories,
	fx.Invoke(handlers.RegisterCreateClassEndpoints),
)
