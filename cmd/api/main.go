package main

import (
	"gitlab.com/felipecaputo/gym-api/cmd/api/modules"
)

func main() {
	app := modules.NewApp(8080)

	app.Run()
}
