//go:build integration
// +build integration

package integrationtest

import (
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/felipecaputo/gym-api/test"
)

type BookClass struct {
	test.E2ESuite
}

func TestSuite(t *testing.T) {
	suite.Run(t, &BookClass{})
}

func (s *BookClass) Test_BookClass_Successfully() {
	const (
		classInput = "{\"name\":\"Yoga\",\"start_date\":\"2022-12-01T00:00:00Z\",\"end_date\":\"2022-12-20T00:00:00Z\", \"capacity\": 20}"

		bookInput  = "{\"user_name\": \"Anakin\", \"class_id\": 1, \"date\": \"2022-12-02T00:00:00Z\"}"
		bookOutput = "{\"id\": 1, \"user_name\": \"Anakin\", \"class_id\": 1, \"date\": \"2022-12-02T00:00:00Z\"}"
	)

	resp, err := s.Client.Post(s.GetURL("/classes"), test.CTJSON, strings.NewReader(classInput))

	s.Nil(err)
	s.Equal(http.StatusCreated, resp.StatusCode)

	resp, err = s.Client.Post(s.GetURL("/bookings"), test.CTJSON, strings.NewReader(bookInput))

	s.Nil(err)
	s.Equal(http.StatusCreated, resp.StatusCode)
	s.JSONEq(bookOutput, s.GetBody(resp))
}

func (s *BookClass) Test_BookClass_BadRequestOnInvalidDate() {
	const (
		classInput = "{\"name\":\"Yoga\",\"start_date\":\"2022-12-01T00:00:00Z\",\"end_date\":\"2022-12-20T00:00:00Z\", \"capacity\": 20}"

		bookInput  = "{\"user_name\": \"Anakin\", \"class_id\": 1, \"date\": \"2023-12-02T00:00:00Z\"}"
		bookOutput = "{\"error\": \"invalid request: validation failed: booking outside class period (2022-12-01, 2022-12-20)\"}"
	)

	resp, err := s.Client.Post(s.GetURL("/classes"), test.CTJSON, strings.NewReader(classInput))

	s.Nil(err)
	s.Equal(http.StatusCreated, resp.StatusCode)

	resp, err = s.Client.Post(s.GetURL("/bookings"), test.CTJSON, strings.NewReader(bookInput))

	s.Nil(err)
	s.Equal(http.StatusBadRequest, resp.StatusCode)
	s.JSONEq(bookOutput, s.GetBody(resp))
}

func (s *BookClass) Test_BookClass_NotFoundOnInvalidClass() {
	const (
		bookInput  = "{\"user_name\": \"Anakin\", \"class_id\": 1, \"date\": \"2022-12-02T00:00:00Z\"}"
		bookOutput = "{\"error\": \"resource not found: class with id 1\"}"
	)

	resp, err := s.Client.Post(s.GetURL("/bookings"), test.CTJSON, strings.NewReader(bookInput))

	s.Nil(err)
	s.Equal(http.StatusNotFound, resp.StatusCode)
	s.JSONEq(bookOutput, s.GetBody(resp))
}
