package bookings

import "time"

type Booking struct {
	ID       int
	ClassID  int `validate:"required,gt=0"`
	Date     time.Time
	UserName string `validate:"required,gt=0"`
}
