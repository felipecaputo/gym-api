package scheduler

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/felipecaputo/gym-api/bookings"
	"gitlab.com/felipecaputo/gym-api/bookings/scheduler/mocks"
	"gitlab.com/felipecaputo/gym-api/classes"
)

func TestService_ScheduleSingleClass(t *testing.T) {
	baseDate := time.Date(2021, 12, 01, 0, 0, 0, 0, time.UTC)
	type fields struct {
		classFinder   ClassFinder
		bookingWriter BookingCreator
	}
	type args struct {
		classID  int
		userName string
		date     time.Time
	}
	tests := []struct {
		name      string
		fields    fields
		args      args
		want      bookings.Booking
		wantedErr string
	}{
		{
			name: "should return zero value on not existent class",
			fields: fields{
				classFinder: buildClassFinder(classes.Class{}, nil),
			},
			args:      args{userName: "aa", classID: 1, date: baseDate.Add(-48 * time.Hour)},
			want:      bookings.Booking{},
			wantedErr: "",
		},
		{
			name: "should return validation error on a class date out of period",
			fields: fields{
				classFinder: buildClassFinder(classes.Class{ID: 1, Name: "Yoga", StartDate: baseDate, EndDate: baseDate.Add(20 * 24 * time.Hour)}, nil),
			},
			args:      args{userName: "aa", classID: 1, date: baseDate.Add(-48 * time.Hour)},
			want:      bookings.Booking{},
			wantedErr: "validation failed: booking outside class period (2021-12-01, 2021-12-21)",
		},
		{
			name: "should return the created booking on success",
			fields: fields{
				classFinder:   buildClassFinder(classes.Class{ID: 1, Name: "Yoga", StartDate: baseDate, EndDate: baseDate.Add(20 * 24 * time.Hour)}, nil),
				bookingWriter: buildBookingCreator(bookings.Booking{ID: 2, UserName: "aa", ClassID: 1, Date: baseDate.Add(48 * time.Hour)}, nil),
			},
			args:      args{userName: "aa", classID: 1, date: baseDate.Add(48 * time.Hour)},
			want:      bookings.Booking{ID: 2, UserName: "aa", ClassID: 1, Date: baseDate.Add(48 * time.Hour)},
			wantedErr: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewService(tt.fields.classFinder, tt.fields.bookingWriter)

			got, err := s.ScheduleSingleClass(tt.args.classID, tt.args.userName, tt.args.date)
			assert.Equal(t, tt.want, got)
			if tt.wantedErr == "" {
				assert.Nil(t, err)
			} else {
				assert.EqualError(t, err, tt.wantedErr)
			}
		})
	}
}

func buildClassFinder(class classes.Class, err error) ClassFinder {
	m := mocks.ClassFinder{}
	m.On("FindByID", mock.AnythingOfType("int")).Return(class, err)

	return &m
}

func buildBookingCreator(booking bookings.Booking, err error) BookingCreator {
	m := mocks.BookingCreator{}
	m.On("Create", mock.AnythingOfType("int"), mock.AnythingOfType("string"), mock.AnythingOfType("time.Time")).Return(booking, err)

	return &m
}
