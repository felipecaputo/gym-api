package scheduler

import (
	"fmt"
	"time"

	"gitlab.com/felipecaputo/gym-api/bookings"
	"gitlab.com/felipecaputo/gym-api/classes"
	"gitlab.com/felipecaputo/gym-api/internal/validator"
)

type (
	ClassFinder interface {
		FindByID(classID int) (classes.Class, error)
	}

	BookingCreator interface {
		Create(classId int, userName string, date time.Time) (bookings.Booking, error)
	}

	Service struct {
		classFinder    ClassFinder
		bookingCreator BookingCreator
	}
)

func NewService(classFinder ClassFinder, bookingCreator BookingCreator) Service {
	return Service{
		bookingCreator: bookingCreator,
		classFinder:    classFinder,
	}
}

// ScheduleSingleClass books a single class for an user, validating period and class
func (s Service) ScheduleSingleClass(classID int, userName string, date time.Time) (bookings.Booking, error) {
	class, err := s.classFinder.FindByID(classID)

	// taking advantage of golang structure and zero value in this scenario we return zero value for classes
	// that were not found and zero value and the error if any error happened
	if err != nil || class.ID == 0 {
		return bookings.Booking{}, err
	}

	if date.Before(class.StartDate) || date.After(class.EndDate) {
		return bookings.Booking{}, validator.NewValidationError(fmt.Errorf("booking outside class period (%s, %s)", class.StartDate.Format("2006-01-02"), class.EndDate.Format("2006-01-02")))
	}

	return s.bookingCreator.Create(classID, userName, date)
}
