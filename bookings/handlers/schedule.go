package handlers

import (
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"gitlab.com/felipecaputo/gym-api/bookings"
	"gitlab.com/felipecaputo/gym-api/internal/httputils"
)

type (
	SchedulerService interface {
		ScheduleSingleClass(classID int, userName string, date time.Time) (bookings.Booking, error)
	}

	Validator interface {
		IsValidationError(err error) bool
	}

	SingleClassHandler struct {
		scheduleService SchedulerService
		validator       Validator
	}

	singleClassRequestDTO struct {
		UserName string    `json:"user_name"`
		ClassID  int       `json:"class_id"`
		Date     time.Time `json:"date"`
	}

	singleClassResponseDTO struct {
		singleClassRequestDTO

		ID int `json:"id"`
	}
)

func NewSingleClassHandler(scheduleService SchedulerService, validator Validator) SingleClassHandler {
	return SingleClassHandler{
		scheduleService: scheduleService,
		validator:       validator,
	}
}

func RegisterScheduleEndpoints(r chi.Router, h SingleClassHandler) {
	r.Post("/bookings", h.ScheduleSingleClass)
}

func (h SingleClassHandler) ScheduleSingleClass(w http.ResponseWriter, r *http.Request) {
	data := singleClassRequestDTO{}
	if err := render.Bind(r, &data); err != nil {
		render.Render(w, r, httputils.ErrBadRequest(err))
		return
	}

	schedule, err := h.scheduleService.ScheduleSingleClass(data.ClassID, data.UserName, data.Date)
	if err != nil && h.validator.IsValidationError(err) {
		render.Render(w, r, httputils.ErrBadRequest(err))
		return
	}

	if err != nil {
		render.Render(w, r, httputils.ErrServerError(err))
		return
	}

	if schedule.ID == 0 {
		render.Render(w, r, httputils.ErrNotFound(fmt.Errorf("class with id %d", data.ClassID)))
		return
	}

	render.Status(r, http.StatusCreated)
	render.Render(w, r, singleClassResponseDTO{
		ID: schedule.ID,
		singleClassRequestDTO: singleClassRequestDTO{
			UserName: schedule.UserName,
			ClassID:  schedule.ClassID,
			Date:     schedule.Date,
		},
	})
}

func (d *singleClassRequestDTO) Bind(r *http.Request) error {
	return nil
}

func (c singleClassResponseDTO) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
