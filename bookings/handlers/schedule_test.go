package handlers

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/felipecaputo/gym-api/bookings"
	"gitlab.com/felipecaputo/gym-api/bookings/handlers/mocks"
)

func TestSingleClassHandler_BookSingleClass(t *testing.T) {
	tests := []struct {
		name            string
		scheduleService SchedulerService
		validator       Validator
		request         string
		wantStatus      int
		wantBody        string
	}{
		{
			name:       "should return bad request when body isn't json",
			request:    "not json",
			wantStatus: http.StatusBadRequest,
			wantBody:   "\"error\":\"invalid request:",
		},
		{
			name:       "should return bad request when request end_date is invalid",
			request:    "{\"user_name\": \"Anakin\", \"class_id\": 1, \"date\": \"invalid_date\"}",
			wantStatus: http.StatusBadRequest,
			wantBody:   "{\"error\":\"invalid request: parsing time \\\"\\\\\\\"invalid_date\\\\\\\"\\\"",
		},
		{
			name:            "should return bad request when creator returns a validation error",
			request:         "{\"name\": \"Yoga\", \"start_date\": \"2021-12-01T00:00:00Z\", \"end_date\": \"2021-12-01T00:00:00Z\", \"capacity\": 20}",
			wantStatus:      http.StatusBadRequest,
			scheduleService: buildSchedulerService(bookings.Booking{}, errors.New("validation error")),
			validator:       buildValidator(true),
			wantBody:        "{\"error\":\"invalid request: validation error",
		},
		{
			name:            "should return internal error when creator failed",
			request:         "{\"name\": \"Yoga\", \"start_date\": \"2021-12-01T00:00:00Z\", \"end_date\": \"2021-12-01T00:00:00Z\", \"capacity\": 20}",
			wantStatus:      http.StatusInternalServerError,
			scheduleService: buildSchedulerService(bookings.Booking{}, errors.New("internal error")),
			validator:       buildValidator(false),
			wantBody:        "{\"error\":\"request failed: internal error\"}\n",
		},
		{
			name:            "should return success and booking data on successfull request",
			request:         "{\"name\": \"Yoga\", \"start_date\": \"2021-12-01T00:00:00Z\", \"end_date\": \"2021-12-01T00:00:00Z\", \"capacity\": 20}",
			wantStatus:      http.StatusCreated,
			scheduleService: buildSchedulerService(bookings.Booking{ID: 1, ClassID: 20, UserName: "Anakin"}, nil),
			wantBody:        "{\"user_name\":\"Anakin\",\"class_id\":20,\"date\":\"0001-01-01T00:00:00Z\",\"id\":1}\n",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			createHandler := NewSingleClassHandler(tt.scheduleService, tt.validator)
			req, err := http.NewRequest("POST", "/bookings", strings.NewReader(tt.request))
			if err != nil {
				t.Fatal(err)
			}

			req.Header.Add("Content-Type", "application/json")

			rr := httptest.NewRecorder()
			handler := http.HandlerFunc(createHandler.ScheduleSingleClass)
			handler.ServeHTTP(rr, req)

			assert.Equal(t, tt.wantStatus, rr.Code)
			assert.Contains(t, rr.Body.String(), tt.wantBody)
		})
	}
}

func buildValidator(isValidationError bool) Validator {
	m := mocks.Validator{}
	m.On("IsValidationError", mock.Anything).Return(isValidationError)

	return &m
}

func buildSchedulerService(booking bookings.Booking, err error) SchedulerService {
	m := mocks.SchedulerService{}
	m.On("ScheduleSingleClass", mock.AnythingOfType("int"), mock.AnythingOfType("string"),
		mock.AnythingOfType("time.Time")).Return(booking, err)

	return &m
}
