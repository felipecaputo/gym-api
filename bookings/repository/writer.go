package repository

import (
	"time"

	"gitlab.com/felipecaputo/gym-api/bookings"
)

type (
	//go:generate mockery --name Validator
	Validator interface {
		Validate(data interface{}) error
	}

	Writer struct {
		validator Validator
		nextID    int
		data      map[int]bookings.Booking
	}
)

func NewWriter(validator Validator) Writer {
	return Writer{
		validator: validator,
		nextID:    1,
		data:      map[int]bookings.Booking{},
	}
}

// Create inserts a new class record into the in memory "database"
func (w *Writer) Create(classId int, userName string, date time.Time) (bookings.Booking, error) {
	booking := bookings.Booking{
		ID:       w.nextID,
		ClassID:  classId,
		UserName: userName,
		Date:     date,
	}

	if err := w.validator.Validate(booking); err != nil {
		return bookings.Booking{}, err
	}

	w.data[w.nextID] = booking
	w.nextID++

	return booking, nil
}
