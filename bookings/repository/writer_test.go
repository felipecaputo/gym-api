package repository

import (
	"errors"
	"reflect"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"gitlab.com/felipecaputo/gym-api/bookings"
	"gitlab.com/felipecaputo/gym-api/bookings/repository/mocks"
)

func TestWriter_Create(t *testing.T) {
	type args struct {
		classID  int
		userName string
		date     time.Time
	}
	tests := []struct {
		name      string
		args      args
		validator Validator
		want      bookings.Booking
		wantErr   bool
	}{
		{
			name:      "should create a class correctly",
			validator: buildValidatorMock(nil),
			args: args{
				userName: "Anakin",
				classID:  21,
				date:     time.Date(2021, 12, 01, 0, 0, 0, 0, time.UTC),
			},
			want: bookings.Booking{
				ID:       1,
				UserName: "Anakin",
				ClassID:  21,
				Date:     time.Date(2021, 12, 01, 0, 0, 0, 0, time.UTC),
			},
			wantErr: false,
		},
		{
			name:      "should return an error if validate fails",
			validator: buildValidatorMock(errors.New("Some error")),
			args: args{
				userName: "Anakin",
				classID:  21,
				date:     time.Date(2021, 12, 01, 0, 0, 0, 0, time.UTC),
			},
			want:    bookings.Booking{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := NewWriter(tt.validator)
			got, err := w.Create(tt.args.classID, tt.args.userName, tt.args.date)
			if (err != nil) != tt.wantErr {
				t.Errorf("Writer.Create() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Writer.Create() = %v, want %v", got, tt.want)
			}
		})
	}
}

func buildValidatorMock(validateResult error) Validator {
	m := &mocks.Validator{}
	m.On("Validate", mock.AnythingOfType("bookings.Booking")).Return(validateResult)

	return m
}
