package validator

import (
	"errors"

	"github.com/go-playground/validator/v10"
)

type Validator struct {
	validator *validator.Validate
}

func NewValidator() Validator {
	return Validator{
		validator: validator.New(),
	}
}

func (v Validator) Validate(data interface{}) error {
	err := v.validator.Struct(data)
	if err == nil {
		return nil
	}

	return NewValidationError(err)
}

func (v Validator) IsValidationError(err error) bool {
	return errors.Is(err, ValidationError{})
}
