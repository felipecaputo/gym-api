package validator

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidator_Validate(t *testing.T) {
	type input struct {
		StrField string `validate:"required,min=3"`
		IntField int    `validate:"gte=10"`
	}
	type args struct {
		data input
	}
	tests := []struct {
		name      string
		args      args
		wantedErr string
	}{
		{
			name:      "should return nil for a valid complete input",
			args:      args{data: input{StrField: "valid", IntField: 11}},
			wantedErr: "",
		},
		{
			name:      "should return error for a required field missing",
			args:      args{data: input{IntField: 11}},
			wantedErr: "invalid field values: StrField: \"\"",
		},
		{
			name:      "should return error for a invalid string length",
			args:      args{data: input{StrField: "a", IntField: 20}},
			wantedErr: "invalid field values: StrField: \"a\"",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := NewValidator()

			err := v.Validate(tt.args.data)
			if err == nil {
				assert.Equal(t, tt.wantedErr, "")
			} else {
				assert.Equal(t, tt.wantedErr, err.Error())
			}
		})
	}
}

func TestValidator_IsValidationError(t *testing.T) {
	type args struct {
		err error
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "should return false for a different error",
			args: args{err: errors.New("other error")},
			want: false,
		},
		{
			name: "should return true for a same type error",
			args: args{err: ValidationError{}},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := NewValidator()

			assert.Equal(t, tt.want, v.IsValidationError(tt.args.err))
		})
	}
}
