package validator

import (
	"fmt"
	"strings"

	"github.com/go-playground/validator/v10"
)

type ValidationError struct {
	err error
}

func NewValidationError(err error) ValidationError {
	return ValidationError{
		err: err,
	}
}

func (v ValidationError) Error() string {
	validationError, ok := v.err.(validator.ValidationErrors)
	if !ok {
		return fmt.Sprintf("validation failed: %s", v.err)
	}

	fieldErrors := make([]string, len(validationError))
	for i, e := range validationError {
		fieldErrors[i] = fmt.Sprintf("%s: \"%v\"", e.Field(), e.Value())
	}

	return fmt.Sprintf("invalid field values: %s", strings.Join(fieldErrors, ","))
}

func (v ValidationError) Is(target error) bool {
	_, ok := target.(ValidationError)
	return ok
}
