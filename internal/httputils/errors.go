package httputils

import (
	"fmt"
	"net/http"

	"github.com/go-chi/render"
)

type HTTPError struct {
	Err        error `json:"-"`
	StatusCode int   `json:"-"`

	ErrorMessage string `json:"error,omitempty"` // the real error message
	ErrorCode    string `json:"code,omitempty"`  // app level error code
}

func (h HTTPError) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, h.StatusCode)
	return nil
}

func ErrBadRequest(err error) HTTPError {
	return HTTPError{
		Err:          err,
		StatusCode:   http.StatusBadRequest,
		ErrorMessage: fmt.Sprintf("invalid request: %s", err),
	}
}

func ErrServerError(err error) HTTPError {
	return HTTPError{
		Err:          err,
		StatusCode:   http.StatusInternalServerError,
		ErrorMessage: fmt.Sprintf("request failed: %s", err),
	}
}

func ErrNotFound(err error) HTTPError {
	return HTTPError{
		Err:          err,
		StatusCode:   http.StatusNotFound,
		ErrorMessage: fmt.Sprintf("resource not found: %s", err),
	}
}
