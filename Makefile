run:
	go run ./cmd/api/*.go

test:
	go test ./... --cover -v

it: 
	go test -tags=integration --cover -v ./...
